//Začetni commit za 6. nalogo
function divElementEnostavniTekst(sporocilo) {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
    return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
var swearWords = {};
function swearWordsToObject(data) {
    var split = data.split('\n');
    for (var i = 0; i < split.length; i++) {
        swearWords[split[i].trim()] = getSwearStars(split[i].trim());
    }
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}
function getSwearStars(data) {
    var temp = "";
    for (var i = 0; i < data.length; i++) {
        temp += "*";
    }
    return temp;
}
function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}
//Zacetna sprememba 1. commit
var dovoljeniSmajliji = {
    ":)": "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">",
    ";)": "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">",
    ":(": "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">",
    ":*": "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">",
    "(y)": "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">"
};

function procesirajVnosUporabnika(klepetApp, socket) {
    var sporocilo = $('#poslji-sporocilo').val();
    var sporocilo2 = $('#poslji-sporocilo').val();
    var sistemskoSporocilo;

    if (sporocilo.charAt(0) == '/') {
        sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
        if (sistemskoSporocilo) {
            $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
        }
    } else {
        for (var smajli in dovoljeniSmajliji) {
            var re = new RegExp(escapeRegExp(smajli), 'g');
            sporocilo = sporocilo.replace(re, dovoljeniSmajliji[smajli]);
        }
        for (var i in swearWords) {
            var re = new RegExp("\\b" + escapeRegExp(i.trim()) + "\\b", 'gi');
            sporocilo = sporocilo.replace(re, swearWords[i]);
            sporocilo2 = sporocilo2.replace(re, swearWords[i]);
        }

        if (sporocilo !== sporocilo2 && sporocilo.indexOf("\<script\>") == -1) {
            klepetApp.posljiSporocilo($('#kanal').text().split('@')[2], sporocilo);
            $('#sporocila').append(divElementHtmlTekst(sporocilo));
        } else {
            klepetApp.posljiSporocilo($('#kanal').text().split('@')[2], sporocilo2);
            $('#sporocila').append(divElementEnostavniTekst(sporocilo2));
        }
        $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
    $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function () {
    var klepetApp = new Klepet(socket);
    $.get("../swearWords.txt", function (response) {
        var logfile = response;
        swearWordsToObject(logfile);
    });
    socket.on('vzdevekSpremembaOdgovor', function (rezultat) {
        var sporocilo;
        if (rezultat.uspesno) {
            sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
            $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
        } else {
            sporocilo = rezultat.sporocilo;
        }
        $('#sporocila').append(divElementHtmlTekst(sporocilo));
    });

    socket.on('pridruzitevOdgovor', function (rezultat) {
        $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
        $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    });

    socket.on('pridruzitevZGeslomNeuspeh', function (kanal) {
        var novElement = $('<div style="font-weight: bold"></div>').text("Pridružitev v kanal " + kanal +
        " ni bilo uspešno, ker je geslo napačno!");
        $('#sporocila').append(novElement);
    });

    socket.on('pridruzitevZGeslomNepotrebno', function (kanal) {
        var novElement = $('<div style="font-weight: bold"></div>').text("Izbrani kanal " + kanal + " je prosto dostopen in ne zahteva prijave z " +
        "geslom, zato se prijavite z uporabo /pridruzitev " +
        kanal + " ali zahtevajte kreiranje kanala z drugim imenom.");
        $('#sporocila').append(novElement);
    });


    socket.on('sporocilo', function (sporocilo) {
        if (sporocilo.besedilo.indexOf("\<script\>") == -1) {
            var novElement = $('<div style="font-weight: bold"></div>').append(divElementHtmlTekst(sporocilo.besedilo));
        } else {
            var novElement = $('<div style="font-weight: bold"></div>').append(divElementEnostavniTekst(sporocilo.besedilo));
        }
        $('#sporocila').append(novElement);
    });

    socket.on('kanali', function (kanali) {
        $('#seznam-kanalov').empty();

        for (var kanal in kanali) {
            kanal = kanal.substring(1, kanal.length);
            if (kanal != '') {
                $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
            }
        }

        $('#seznam-kanalov div').click(function () {
            klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
            $('#poslji-sporocilo').focus();
        });
    });

    socket.on('uporabniki', function (uporabniki) {
        $('#seznam-uporabnikov').empty();
        if (uporabniki != null) {
            for (var i = 0; i < uporabniki.length; i++) {
                if (uporabniki[i] != null) {
                    $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
                }
            }
        }
    });
    setInterval(function () {
        socket.emit('kanali');
        socket.emit('uporabniki');
    }, 1000);

    $('#poslji-sporocilo').focus();

    socket.on('zasebno', function (sporocilo) {
        var novElement = $('<div style="font-weight: bold;color: blue"></div>').text("Zasebno sporočilo od " +
        sporocilo.posiljatelj + " -> " + sporocilo.besedilo);
        $('#sporocila').append(novElement);
    });

    socket.on('zasebnoNapaka', function (sporocilo) {
        var novElement = $('<div style="color: red"></div>').text("Sporočilo " + sporocilo.besedilo
        + " uporabniku z vzdevkom " + sporocilo.prejemnik
        + " ni bilo mogoče posredovati.");
        $('#sporocila').append(novElement);
    });
    socket.on('zasebnoUspesno', function (sporocilo) {
        var novElement = $('<div style="color: green"></div>').text("Sporočilo " + sporocilo.besedilo
        + " uporabniku z vzdevkom " + sporocilo.prejemnik
        + " je bilo uspesno posredovano.");
        $('#sporocila').append(novElement);
    });

    $('#poslji-obrazec').submit(function () {
        procesirajVnosUporabnika(klepetApp, socket);
        return false;
    });
});