var Klepet = function (socket) {
    this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function (kanal, besedilo) {
    var sporocilo = {
        kanal: kanal,
        besedilo: besedilo
    };
    this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function (kanal) {
    this.socket.emit('pridruzitevZahteva', {
        novKanal: kanal
    });
};

Klepet.prototype.procesirajUkaz = function (ukaz) {
    var besede = ukaz.split(' ');
    ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
    var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
        besede.shift();
        besede = besede.join(' ');
        if(besede.indexOf('"') == -1){
            this.spremeniKanal(besede);
        }else{
            besede = besede.split('"');
            var kanal = besede[1];
            var geslo = besede[3];
            this.socket.emit('pridruzitevZGeslomZahteva', {kanal: kanal, geslo: geslo});
        }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var prejemnikTmp = besede.shift();
      var prejemnik = prejemnikTmp.substring(1, prejemnikTmp.length-1);
      var tekstTmp = besede.join(' ');
      var tekst = tekstTmp.substring(1, tekstTmp.length-1);
      this.socket.emit('zasebno', {besedilo: tekst, prejemnik: prejemnik});
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

    return sporocilo;
};