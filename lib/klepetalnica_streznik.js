var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var vzdevkiGledeNaKanal = {};
var kanaliZGesli = {};
//Začetna sprememba za 7. nalogo

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function() {
      socket.emit('uporabniki', vzdevkiGledeNaKanal[trenutniKanal[socket.id]]);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function obdelajZasebnoSporocilo(socket) {
  socket.on('zasebno', function (sporocilo) {
    if (uporabljeniVzdevki.indexOf(sporocilo.prejemnik) != -1 &&
        sporocilo.prejemnik !== vzdevkiGledeNaSocket[socket.id] && sporocilo.besedilo !== "") {
      for (var i in vzdevkiGledeNaSocket) {
        if (vzdevkiGledeNaSocket[i] === sporocilo.prejemnik) {
          io.sockets.socket(i).emit('zasebno',
              {posiljatelj: vzdevkiGledeNaSocket[socket.id], besedilo: sporocilo.besedilo});
          io.sockets.socket(socket.id).emit('zasebnoUspesno', {
            besedilo: sporocilo.besedilo,
            prejemnik: sporocilo.prejemnik
          });
          break;
        }
      }
    } else {
      io.sockets.socket(socket.id).emit('zasebnoNapaka', {
        besedilo: sporocilo.besedilo,
        prejemnik: sporocilo.prejemnik
      });
    }
  });
}
function obdelajPridruzitevKanaluZGeslom(socket, kanal, geslo){
  var kanali = Object.keys(kanaliZGesli);
  var indexKanala = kanali.indexOf(kanal);
  if(indexKanala == -1){
    kanaliZGesli[kanal] = geslo;
    socket.leave(trenutniKanal[socket.id]);
    trenutniKanal[socket.id] = kanal;
    socket.join(kanal);
    socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek: vzdevkiGledeNaSocket[socket.id]});
    socket.broadcast.to(kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
    });
    var temp = [];
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    if (uporabnikiNaKanalu.length > 0) {
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        var upIme = vzdevkiGledeNaSocket[uporabnikSocketId];
        temp.push(upIme);
      }
      vzdevkiGledeNaKanal[kanal] = temp;
    }
  }else{
    if(kanaliZGesli[kanali[indexKanala]] === geslo){
      socket.leave(trenutniKanal[socket.id]);
      trenutniKanal[socket.id] = kanal;
      socket.join(kanal);
      socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek: vzdevkiGledeNaSocket[socket.id]});
      socket.broadcast.to(kanal).emit('sporocilo', {
        besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
      });
      var temp = [];
      var uporabnikiNaKanalu = io.sockets.clients(kanal);
      if (uporabnikiNaKanalu.length > 0) {
        for (var i in uporabnikiNaKanalu) {
          var uporabnikSocketId = uporabnikiNaKanalu[i].id;
          var upIme = vzdevkiGledeNaSocket[uporabnikSocketId];
          temp.push(upIme);
        }
        vzdevkiGledeNaKanal[kanal] = temp;
      }
    }else{
      io.sockets.socket(socket.id).emit('pridruzitevZGeslomNeuspeh', kanal);
    }
  }
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });

  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

//Sprememba za zacetni commit 2. naloge

function pridruzitevKanalu(socket, kanal) {
  var temp = [];
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek: vzdevkiGledeNaSocket[socket.id]});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
  //Zacetna sprememba za 5. nalogo
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 0) {
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      var upIme = vzdevkiGledeNaSocket[uporabnikSocketId];
      temp.push(upIme);
    }
    vzdevkiGledeNaKanal[kanal] = temp;
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        console.log(vzdevkiGledeNaKanal[trenutniKanal[socket.id]]);
        for(var i = 0; i < vzdevkiGledeNaKanal[trenutniKanal[socket.id]].length; i++){
          if(vzdevkiGledeNaKanal[trenutniKanal[socket.id]][i] === uporabljeniVzdevki[prejsnjiVzdevekIndeks]){
            vzdevkiGledeNaKanal[trenutniKanal[socket.id]][i] = vzdevek;
            break;
          }
        }
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var kanali = Object.keys(kanaliZGesli);
    if(kanali.indexOf(kanal.novKanal) != -1){
      io.sockets.socket(socket.id).emit('pridruzitevZGeslomNeuspeh', kanal.novKanal);
    }else{
      socket.leave(trenutniKanal[socket.id]);
      for(var i = 0; i < vzdevkiGledeNaKanal[trenutniKanal[socket.id]].length; i++){
        if(vzdevkiGledeNaKanal[trenutniKanal[socket.id]][i] === vzdevkiGledeNaSocket[socket.id]){
          delete vzdevkiGledeNaKanal[trenutniKanal[socket.id]][i];
          break;
        }
      }
      pridruzitevKanalu(socket, kanal.novKanal);
    }
  });
  socket.on('pridruzitevZGeslomZahteva', function(data){
    obdelajPridruzitevKanaluZGeslom(socket, data.kanal, data.geslo);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('disconnect', function() {
    for(var i = 0; i < vzdevkiGledeNaKanal[trenutniKanal[socket.id]].length; i++){
      if(vzdevkiGledeNaKanal[trenutniKanal[socket.id]][i] === vzdevkiGledeNaSocket[socket.id]){
        vzdevkiGledeNaKanal[trenutniKanal[socket.id]].splice(i, i);
        break;
      }
    }
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}